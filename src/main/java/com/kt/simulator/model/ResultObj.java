package com.kt.simulator.model;

import java.util.Map;

public class ResultObj {
	int resultCode = 100;
	int requestSeq;
	Object resultData;

	public ResultObj(Map<String, Object> param) {
		try {
			setRequestSeq(Integer.valueOf((String) param.get("requestSeq")));
		} catch (Exception e) {
			setRequestSeq(0);
		}
	}

	public ResultObj() {
	}

	public int getRequestSeq() {
		return requestSeq;
	}

	public void setRequestSeq(int requestSeq) {
		this.requestSeq = requestSeq;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public Object getResultData() {
		return resultData;
	}

	public void setResultData(Object resultData) {
		this.resultData = resultData;
	}

}