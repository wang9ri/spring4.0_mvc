package com.kt.simulator.persistence;

import java.util.List;
import java.util.Map;

public interface UserMapper {
	Integer insert(Map<String, Object> param);
	Integer update(Map<String, Object> param);
	Integer delete(Map<String, Object> param);
	List<Map<String, Object>> select(Map<String, Object> param);
	Integer preDelete(Map<String, Object> param);
	Integer claimInsert(Map<String, Object> param);
}