package com.kt.simulator.controller;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.simulator.model.ResultObj;

@Controller
public class ErrorController {
	@RequestMapping(value="/error", produces="application/json")
	@ResponseBody 
	public ResultObj handle(HttpServletRequest request){
		ResultObj resultObj = new ResultObj();
		resultObj.setResultCode(500);
		resultObj.setResultData(request.getAttribute("javax.servlet.error.status_code")+"에러가 발생하였습니다.");
		return resultObj;
	}
}