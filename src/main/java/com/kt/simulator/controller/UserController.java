package com.kt.simulator.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.simulator.model.ResultObj;
import com.kt.simulator.persistence.UserMapper;

/**
 * Handles requests for the application home page.
 */
@Controller
public class UserController {

	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);

	@Autowired
	UserMapper userMapper;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "home";
	}

	@RequestMapping(value = "/user/select", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj userSelect(@RequestParam Map<String, Object> param) {
		logger.debug("[" + param + "]");
		ResultObj resultObj = new ResultObj(param);
		List<Map<String, Object>> result = userMapper.select(param);
		resultObj.setResultCode(300);
		resultObj.setResultData(result);
		return resultObj;
	}

	/*
	@RequestMapping(value = "/user/login", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj userLogin(@RequestParam Map<String, Object> param) {
		logger.debug("[" + param + "]");
		ResultObj resultObj = new ResultObj(param);
		List<Map<String, Object>> result = userMapper.select(param);
		resultObj.setResultCode(300);
		resultObj.setResultData(result);
		return resultObj;
	}

	
	
	@RequestMapping(value = "/user/delete", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj userDelete(@RequestParam Map<String,Object> param) {
		logger.debug("["+param+"]");
		ResultObj resultObj = new ResultObj(param); 
		Integer result = userMapper.delete(param);
		if (result > 0) {
			resultObj.setResultCode(300);
			resultObj.setResultData(param);
		}else{
			resultObj.setResultCode(500);
		}
		return resultObj;
	}
	
	@RequestMapping(value = "/user/update", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj userUpdate(@RequestParam Map<String,Object> param) {
		logger.debug("["+param+"]");
		ResultObj resultObj = new ResultObj(param); 
		Integer result = userMapper.update(param);
		if (result > 0) {
			resultObj.setResultCode(300);
			resultObj.setResultData(param);
		}else{
			resultObj.setResultCode(500);
		}
		return resultObj;
	}

	@RequestMapping(value = "/user/claimInsert", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj claimInsert(@RequestParam Map<String,Object> param) {
		logger.debug("["+param+"]");
		ResultObj resultObj = new ResultObj(param);  
		Integer result = userMapper.claimInsert(param);
		if (result > 0) {
			resultObj.setResultCode(300);
			resultObj.setResultData(param);
		}else{
			resultObj.setResultCode(500);
		}
		return resultObj;
	}

	@RequestMapping(value = "/user/insert", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj userInsert(@RequestParam Map<String,Object> param) {
		logger.debug("["+param+"]");
		ResultObj resultObj = new ResultObj(param); 
		Integer result = userMapper.insert(param);
		if (result > 0) {
			resultObj.setResultCode(300);
			resultObj.setResultData(param);
		}else{
			resultObj.setResultCode(500);
		}
		return resultObj;
	}

	@RequestMapping(value = "/user/insertUpdate", method = RequestMethod.GET)
	@ResponseBody
	public ResultObj userInsertUpdate(@RequestParam Map<String,Object> param) {
		logger.debug("["+param+"]");
		ResultObj resultObj = new ResultObj(param);
		Integer resultInsert = 0;
		try {
			resultInsert = userMapper.insert(param);
			Integer resultDelete = userMapper.preDelete(param);
			Integer resultDelete2 = ((UserMapper) param).delete(param);
		} catch (Exception e) {
			resultObj.setResultCode(500);
			resultObj.setResultData(e.getMessage());
			return resultObj;
		}
		
		if (resultInsert > 0) {
			resultObj.setResultCode(300);
			resultObj.setResultData(param);
		}else{
			resultObj.setResultCode(500);
		}
		return resultObj;
	}
	
	*/
	
}
